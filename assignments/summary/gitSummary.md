# GIT

## Introduction
**Git** is the most commonly used version control system. 

<img src="https://www.nobledesktop.com/image/blog/git-branches-merge.png" width="400"  heigth="400">

## How to use Git?
Git can be accessed by
1. Command line (Terminal), or
2. Desktop app which has a GUI 


## The Three States
Git has three main states that your files can reside in: modified, staged, and committed.
* **Modified**: The file has been changed but have not committed it to the database yet.
* **Staged**: The modified file is marked in its current version to go into the next commit snapshot.
* **Committed** : Data is safely stored in local repo in form of
pictures/snapshots

This leads us to the three main sections of a Git project: 
* _Working tree_
* _Staging area_
* _Git directory_

<img src="https://git-scm.com/book/en/v2/images/areas.png" height="300" >


## How to get started?

### Download Git
The following link can be used to install Git in multiple OS-
[](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

To verify Git is installed, type the following command in command promt<br>

<code>git --version</code>

Also, create an account in GitHub and create a project to get started.

###  Clone the repo
A Git repository (or repo for short) contains all of the project files and the entire revision history. 

Repo link can be found on the main page.
<img src="https://help.github.com/assets/images/help/repository/clone-repo-clone-url-button.png">

<img src="https://help.github.com/assets/images/help/repository/https-url-clone.png">

Use the following in commad line to clone the repo to your local system<br>
<code>git clone <repository-link>  </code>
	
### Create a new branch
The command used to create a new branch<br>
	<code>git checkout -b <branch-name> </code>
		
### Adding files to staging area
After modifying the files, they need to be staged.<br>
		<code> git add .  </code> 
		
### Commiting Files
Commit takes the files as they are in the staging area and stores that snapshot permanently to your Local Git Repository.<br>
		<code> git commit -m "<message>" </code>

			
### Push
In order to push all the code from the local repository into the remote repository, use the following command:<br>
			<code>git push</code>
			
### Pull
Pull is used pull the latest changes from the remote repository into the local repository. <br>
			<code> git pull</code>


