# Docker
Docker is a platform for developers and sysadmins to build, run, and share applications with containers.
The use of containers to deploy applications is called containerization. 

## Containers and Virtual Machines
A container runs natively on Linux and shares the kernel of the host machine with other containers. It runs a discrete process and is lightweight.<br>
<img src="https://docs.docker.com/images/Container%402x.png" width="300" height="300">


A virtual machine (VM) runs a full-blown “guest” operating system with virtual access to host resources through a hypervisor.<br>
<img src="https://docs.docker.com/images/VM%402x.png" width="300" height="300">


## Image and Containers
A container is nothing but a running process, with some added encapsulation features applied to it in order to keep it isolated from the host and from other containers. 

A Docker image includes everything needed to run an application - the code or binary, runtimes, dependencies, and any other filesystem objects required.

## Components of Docker
### Docker Engine
It is responsible for the overall functioning of the Docker platform. It is a client-server based application and consists of 3 main components.
* **Server:** It is responsible for creating and managing Docker Images, Containers, Networks and Volumes on the Docker platform.
* **REST API:** The REST API specifies how the applications can interact with the Server, and instruct it to get their job done.
* **Client:** The Client is nothing but a command-line interface, that allows users to interact with Docker using the commands.<br>
<img src="https://docs.docker.com/engine/images/engine-components-flow.png" width="400" height="300">
<br>

### Docker Hub
Docker Hub is the official online repository where you could find all the Docker Images that are available for us to use.

## Set up your Docker environment
### 1.Download and install Docker Desktop
Download and install Docker Desktop appropriate for your operating system

### 2.Test Docker version
In command line, check the version using the command.
<code>docker --version</code>

### 3. Test Docker installation
1. Test that your installation works by running the hello-world Docker image:
<code>docker run hello-world</code>

2. List the hello-world image that you downloaded to your machine.
<code>docker image ls</code>

3.List the hello-world container (spawned by the image) which exits after displaying its message.
<code>docker ps --all</code>
<br>
At this point, Docker Desktop has been installed on development machine.

## Build and Run your Image
### 1. Define a container with Dockerfile
Writing a Dockerfile is the first step to containerizing an application. You can think of these Dockerfile commands as a step-by-step recipe on how to build up your image. This one takes the following steps:

* Start <code>FROM</code> the pre-existing <code>node:current-slim image</code>. This is an official image, built by the node.js vendors and validated by Docker to be a high-quality image containing the Node.js Long Term Support (LTS) interpreter and basic dependencies.
* Use <code>WORKDIR</code> to specify that all subsequent actions should be taken from the directory /usr/src/app in your image filesystem (never the host’s filesystem).
* <code> COPY</code> the file package.json from your host to the present location (.) in your image (so in this case, to /usr/src/app/package.json)
* <code>RUN</code> the command npm install inside your image filesystem (which will read package.json to determine your app’s node dependencies, and install them)
* <code>COPY</code> in the rest of your app’s source code from your host to your image filesystem

### 2. Build and test your image
Now that you have some source code and a Dockerfile, it’s time to build your first image, and make sure the containers launched from it work as expected.

Make sure you’re in the directory node-bulletin-board/bulletin-board-app in a terminal or PowerShell using the cd command. Let’s build your bulletin board image:
<code>docker build --tag bulletinboard:1.0</code>

### 3. Run your image as a container
1. Start a container based on your new image:
<code>docker run --publish 8000:8080 --detach --name bb bulletinboard:1.0</code>
2.Visit your application in a browser at localhost:8000. You should see your bulletin board application up and running. 
3. Once you’re satisfied that your bulletin board container works correctly, you can delete it:
<code>docker rm --force bb</code>

At this point, you’ve successfully built an image, performed a simple containerization of an application, and confirmed that your app runs successfully in its container.

## Share images on Docker Hub
### 1. Set up your Docker Hub account
Set up a Docker ID to share images on Docker Hub.

### 2. Create a Docker Hub repository and push your image
1. Click on the Docker icon in your menu bar, and navigate to <code>Repositories > Create</code>. You’ll be taken to a Docker Hub page to create a new repository.

2. Fill out the repository name as bulletinboard. Leave all the other options alone for now, and click Create at the bottom.
<img src="https://docs.docker.com/get-started/images/newrepo.png" width="700" height="500">

3. Now you are ready to share your image on Docker Hub, but there’s one thing you must do first: images must be namespaced correctly to share on Docker Hub.
<code>docker tag bulletinboard:1.0 gordon/bulletinboard:1.0</code>
4.Finally, push your image to Docker Hub:
<code>docker push gordon/bulletinboard:1.0</code>
 
Now that your image is available on Docker Hub, you’ll be able to run it anywhere.
